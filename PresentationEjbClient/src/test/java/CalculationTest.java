import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.scholanova.eial.archdist.PresentationEjb.Calcul;

public class CalculationTest {
	
@Test
public void testA() {
	//given
	int x = 81;
	int y = 9;
	int z = 90;
	Calcul calc = new Calcul();
	//when
	int res_to_test = calc.ajouter(x, y);
	//then
	assertEquals(res_to_test, z);
}

@Test
public void testB() {
	//given
		int x = 0;
		int y = 0;
		int z = 0;
		Calcul calc = new Calcul();
		//when
		int res_to_test = calc.ajouter(x, y);
		//then
		assertEquals(res_to_test, z);
}

@Test
public void testC() {
	//given
			int x = 1;
			int y = 2;
			int z = 3;
			Calcul calc = new Calcul();
			//when
			int res_to_test = calc.ajouter(x, y);
			//then
			assertEquals(res_to_test, z);
}

}
